﻿using System;
namespace Location.Droid
{
    public static class Constants
    {
        public const int DELAY_BETWEEN_LOG_MESSAGES = 5*1000; // milliseconds
        public const int SERVICE_RUNNING_NOTIFICATION_ID = 10*1000; // milliseconds
        public const int REQUEST_LOCATION_UPDATES_INTERVAL = 10*1000; // seconds * milliseconds
        public const int DELAY_BETWEEN_GPS_LOCATION_REQUESTS = 10*1000; // seconds * milliseconds
        public const int DELAY_INIT_GPS_LOCATION_REQUESTS = 1 * 1000; // seconds * milliseconds
        public const int DELAY_BETWEEN_NETWORK_LOCATION_REQUESTS = 60*1000; // seconds * milliseconds
        public const int DELAY_INIT_NETWORK_LOCATION_REQUESTS = 2*1000; // seconds * milliseconds
        public const int RESTART_LOCATION_SERVICE_IF_IDLE_FOR_MORE_THAN = 120; // seconds
        public const int REPORT_LOCATIONS_INTERVAL = 5; // minutes
        public const string SERVICE_STARTED_KEY = "has_service_been_started";
        public const string BROADCAST_MESSAGE_KEY = "broadcast_message";
        public const string NOTIFICATION_BROADCAST_ACTION = "Location.Droid.Notification.Action";

        public const string ACTION_START_SERVICE = "Location.Droid.action.START_SERVICE";
        public const string ACTION_STOP_SERVICE = "Location.Droid.action.STOP_SERVICE";
        public const string ACTION_RESTART_TIMER = "Location.Droid.action.RESTART_TIMER";
        public const string ACTION_MAIN_ACTIVITY = "Location.Droid.action.MAIN_ACTIVITY";
    }
}
