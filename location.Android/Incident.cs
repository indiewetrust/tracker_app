﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace Location.Droid
{
    class Incident
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public Double latitude { get; set; }
        public Double longitude { get; set; }
        public String type { get; set; }
        public String description { get; set; }
        public String status { get; set; }
        public DateTime created_at { get; set; }
    }
}