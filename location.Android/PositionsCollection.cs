﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Location.Droid
{
    class PositionsCollection
    {
        public String token { get; set; }
        public List<CleanPosition> positions { get; set; }
    }

    class IncidentsCollection
    {
        public String token { get; set; }
        public List<CleanIncident> incidents { get; set; }
    }

    class CleanPosition
    {
        public Double latitude { get; set; }
        public Double longitude { get; set; }
        public int accuracy { get; set; }
        public Double speed { get; set; }
        public int battery { get; set; }
        public String provider { get; set; }
        public DateTime created_at { get; set; }
    }

    class CleanIncident
    {
        public Double latitude { get; set; }
        public Double longitude { get; set; }
        public String type { get; set; }
        public String description { get; set; }
        public DateTime created_at { get; set; }
    }
}