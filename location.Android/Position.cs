﻿using System;
using SQLite;

namespace Location.Droid
{
    class Position
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public Double latitude { get; set; }
        public Double longitude { get; set; }
        public Double accuracy { get; set; }
        public Double speed { get; set; }
        public String provider { get; set; }
        public int battery { get; set; }
        public String status { get; set; }
        public DateTime created_at { get; set; }
    }
}