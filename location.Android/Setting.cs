﻿using System;
using SQLite;

namespace Location.Droid
{
    class Setting
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public Double version { get; set; }
        public String userName { get; set; }
        public String userPass { get; set; }
        public String userToken { get; set; }
        public DateTime? last_specialist_update { get; set; }
    }
}