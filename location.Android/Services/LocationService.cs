using System;
using Android.App;
using Android.Util;
using Android.Content;
using Android.OS;
using Android.Locations;

namespace Location.Droid.Services
{
	[Service]
	public class LocationService : Service, ILocationListener
	{

		public event EventHandler<LocationChangedEventArgs> LocationChanged = delegate { };
		public event EventHandler<ProviderDisabledEventArgs> ProviderDisabled = delegate { };
		public event EventHandler<ProviderEnabledEventArgs> ProviderEnabled = delegate { };
		public event EventHandler<StatusChangedEventArgs> StatusChanged = delegate { };

		public LocationService() 
		{
		}

		protected LocationManager LocMgr = Android.App.Application.Context.GetSystemService ("location") as LocationManager;
        Handler handlerCoarse;
        //Handler handlerFine;
        Action getCoarseLocation;
        //Action getFineLocation;

        readonly string logTag = "LocationService";
		IBinder binder;

        public override void OnCreate ()
		{
			base.OnCreate ();
            handlerCoarse = new Handler();
            getCoarseLocation = new Action(() =>
            {
                Criteria coarseCriteria = new Criteria();
                coarseCriteria.Accuracy = Accuracy.Coarse;
                coarseCriteria.PowerRequirement = Power.NoRequirement;
                LocMgr.RequestSingleUpdate(coarseCriteria, this, null);
                handlerCoarse.PostDelayed(getCoarseLocation, Constants.DELAY_BETWEEN_NETWORK_LOCATION_REQUESTS);
            });
            //handlerFine = new Handler();
            //getFineLocation = new Action(() =>
            //{
            //    Criteria fineCriteria = new Criteria();
            //    fineCriteria.Accuracy = Accuracy.Fine;
            //    fineCriteria.PowerRequirement = Power.NoRequirement;
            //    LocMgr.RequestSingleUpdate(fineCriteria, this, null);
            //    handlerFine.PostDelayed(getFineLocation, Constants.DELAY_BETWEEN_GPS_LOCATION_REQUESTS);
            //});
        }

        // This gets called when StartService is called in our App class
        [Obsolete("deprecated in base class")]
        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            Log.Debug(logTag, "LocationService started");
            return StartCommandResult.Sticky;
        }

		public override IBinder OnBind (Intent intent)
		{
			Log.Debug (logTag, "Client now bound to service");
			binder = new LocationServiceBinder (this);
			return binder;
		}

        public void GetCurrentLocation()
        {
            Criteria fineCriteria = new Criteria();
            fineCriteria.Accuracy = Accuracy.Fine;
            fineCriteria.PowerRequirement = Power.NoRequirement;
            String fineProvider = LocMgr.GetBestProvider(fineCriteria, true);
            LocMgr.RequestLocationUpdates(fineProvider, Constants.REQUEST_LOCATION_UPDATES_INTERVAL, 0, this);
            handlerCoarse.PostDelayed(getCoarseLocation, Constants.DELAY_INIT_NETWORK_LOCATION_REQUESTS);
            //handlerFine.PostDelayed(getFineLocation, Constants.DELAY_INIT_GPS_LOCATION_REQUESTS);
        }

		public override void OnDestroy ()
		{
			base.OnDestroy ();
            LocMgr.RemoveUpdates(this);
		}

		#region ILocationListener implementation

		public void OnLocationChanged (Android.Locations.Location location)
		{
			this.LocationChanged (this, new LocationChangedEventArgs (location));
        }

		public void OnProviderDisabled (string provider)
		{
			this.ProviderDisabled (this, new ProviderDisabledEventArgs (provider));
        }

		public void OnProviderEnabled (string provider)
		{
			this.ProviderEnabled (this, new ProviderEnabledEventArgs (provider));
		}

		public void OnStatusChanged (string provider, Availability status, Bundle extras)
		{
			this.StatusChanged (this, new StatusChangedEventArgs (provider, status, extras));
		} 

		#endregion

	}
}

