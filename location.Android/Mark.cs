﻿using System;
using SQLite;

namespace Location.Droid
{
    class Mark
    {
        public Double latitude { get; set; }
        public Double longitude { get; set; }
        public String type { get; set; }
        public String name { get; set; }
        public String description { get; set; }
        public String address { get; set; }
        public DateTime created_at { get; set; }
    }
}