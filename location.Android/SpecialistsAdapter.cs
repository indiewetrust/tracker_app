﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace Location.Droid
{
    class SpecialistsAdapter : BaseAdapter
    {
        Activity context;

        public List<MainActivity._specialist> _specialists;
        readonly string dbFile = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "T07.db");

        public SpecialistsAdapter(Activity context) //We need a context to inflate our row view from 
            : base()
        {
            this.context = context;

            //For demo purposes we hard code some data here 
            //this._specialists = new List<Specialist>() {
            //    new Specialist() { id=1, name = "Adam", surname="Słodowy", description="ajkshdi uiajhsduf kjasd..." },
            //    new Specialist() { id=2, name = "Józef", surname="Stalin", description="ajkshdi uiajhsduf kjasd..." },
            //    new Specialist() { id=3, name = "Adolf", surname="Hitler", description="ajkshdi uiajhsduf kjasd...", is_target=true },
            //    new Specialist() { id=4, name = "Adolf", surname="Hitler", description="ajkshdi uiajhsduf kjasd..." },
            //    new Specialist() { id=5, name = "Adolf", surname="Hitler", description="ajkshdi uiajhsduf kjasd..." },
            //    new Specialist() { id=6, name = "Adolf", surname="Hitler", description="ajkshdi uiajhsduf kjasd...",scheduled_at=DateTime.Now },
            //    new Specialist() { id=7, name = "Adolf", surname="Hitler", description="ajkshdi uiajhsduf kjasd..." },
            //    new Specialist() { id=8, name = "Adolf", surname="Hitler", description="ajkshdi uiajhsduf kjasd..." },
            //    new Specialist() { id=9, name = "Adolf", surname="Hitler", description="ajkshdi uiajhsduf kjasd...", scheduled_at=DateTime.Now, is_target = true },
            //    new Specialist() { id=10, name = "Adolf", surname="Hitler", description="ajkshdi uiajhsduf kjasd..." },
            //    new Specialist() { id=11, name = "Adolf", surname="Hitler", description="ajkshdi uiajhsduf kjasd..." },
            //    new Specialist() { id=12, name = "Adolf", surname="Hitler", description="ajkshdi uiajhsduf kjasd..." },
            //    new Specialist() { id=13, name = "Adolf", surname="Hitler", description="ajkshdi uiajhsduf kjasd..." },
            //    new Specialist() { id=14, name = "Adolf", surname="Hitler", description="ajkshdi uiajhsduf kjasd..." },
            //    new Specialist() { id=15, name = "Adolf", surname="Hitler", description="ajkshdi uiajhsduf kjasd..." },
            //};

            _specialists = getSpecialistsAsync();
        }

        private List<MainActivity._specialist> getSpecialistsAsync()
        {
            try
            {
                var db = new SQLiteConnection(dbFile);
                List<MainActivity._specialist> specialists = db.Table<MainActivity._specialist>().Take(20).ToList();
                return specialists;
            }
            catch (Exception e)
            {
                return new List<MainActivity._specialist>();
            }
        }

        public override int Count
        {
            get { return _specialists.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            //Get our object for this position
            var item = _specialists[position];

            //Try to reuse convertView if it's not null, otherwise inflate it from our item layout
            // This gives us some performance gains by not always inflating a new view
            // This will sound familiar to MonoTouch developers with UITableViewCell.DequeueReusableCell()
            var view = (convertView ?? context.LayoutInflater.Inflate(Resource.Layout.speclistitem, parent, false)) as LinearLayout;

            //Find references to each subview in the list item's view
            var textDate = view.FindViewById(Resource.Id.textDate) as TextView;
            var textTop = view.FindViewById(Resource.Id.textTop) as TextView;
            var textBottom = view.FindViewById(Resource.Id.textBottom) as TextView;
            var textTarget = view.FindViewById(Resource.Id.textTarget) as TextView;

            //Assign this item's values to the various subviews 
            textTop.SetText(item.name+" "+item.surname, TextView.BufferType.Normal);
            textBottom.SetText(item.description, TextView.BufferType.Normal);
            //textTarget.SetText((item.is_target) ? "Target" : "",TextView.BufferType.Normal);
            //textDate.SetText((item.scheduled_at!=null)?Convert.ToDateTime(item.scheduled_at).ToShortDateString():"", TextView.BufferType.Normal);

            //Finally return the view 
            return view;
        }

        public MainActivity._specialist GetItemAtPosition(int position)
        {
            return _specialists[position];
        }
    }

    //class Specialist
    //{
    //    public int id { get; set; }
    //    public string name { get; set; }
    //    public string surname { get; set; }
    //    public string description { get; set; }
    //    public string status { get; set; }
    //    public bool? is_target { get; set; }
    //    public DateTime? scheduled_at { get; set; }
    //    public DateTime? completed_at { get; set; }
    //}
}