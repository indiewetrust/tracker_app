﻿using Android.App;
using Android.OS;
using Android.Widget;
using Android.Locations;
using System;
using Android.Runtime;
using Android.Util;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;
using System.Text;
using System.Collections;

namespace Location.Droid
{
    [Activity(Label = "Tracker - raportowanie")]
    public class MarkActivity : Activity, ILocationListener
    {
        String placeTypeText = "Lekarz";
        TextView placeTypeLabel;
        EditText placeType;
        TextView placeAddressLabel;
        TextView placeAddress;
        TextView placeLoadingLabel;
        ProgressBar placeLoading;
        TextView placeNameLabel;
        EditText placeName;
        Button placeMarkButton;
        Double placeLatitude;
        Double placeLongitude;
        Spinner spinnerPeople;

        String userToken;
        String passedPlaceType;

        protected LocationManager LocMgr = Android.App.Application.Context.GetSystemService("location") as LocationManager;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Mark);

            userToken = Intent.GetStringExtra("userToken") ?? "empty";
            passedPlaceType = Intent.GetStringExtra("placeType") ?? "empty";
            placeTypeText = passedPlaceType;

            placeTypeLabel = FindViewById<TextView>(Resource.Id.placeTypeLabel);
            placeType = FindViewById<EditText>(Resource.Id.placeType);
            placeAddressLabel = FindViewById<TextView>(Resource.Id.placeAddressLabel);
            placeAddress = FindViewById<TextView>(Resource.Id.placeAddress);
            placeLoadingLabel = FindViewById<TextView>(Resource.Id.placeLoadingLabel);
            placeLoading = FindViewById<ProgressBar>(Resource.Id.placeLoading);
            placeNameLabel = FindViewById<TextView>(Resource.Id.placeNameLabel);
            placeName = FindViewById<EditText>(Resource.Id.placeName);
            placeMarkButton = FindViewById<Button>(Resource.Id.placeMarkButton);
            //spinnerPeople = FindViewById<Spinner>(Resource.Id.spinnerPeople);

            var adapter = ArrayAdapter.CreateFromResource(this, Resource.Array.place_types_array, Android.Resource.Layout.SimpleSpinnerItem);
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);

            placeTypeLabel.Text = "Rodzaj miejsca";
            placeType.Text = passedPlaceType;
            placeType.Enabled = false;
            placeType.ClearFocus();
            placeName.RequestFocusFromTouch();
            // Create your application here
            placeMarkButton.SetBackgroundColor(Android.Graphics.Color.ForestGreen);

            Criteria coarseCriteria = new Criteria();
            coarseCriteria.Accuracy = Accuracy.Coarse;
            coarseCriteria.PowerRequirement = Power.NoRequirement;
            LocMgr.RequestSingleUpdate(coarseCriteria, this, null);
            hideMarkPanel();
            showLoadPanel();
            //getSpecialists();
            placeMarkButton.Click += PlaceMarkButton_Click;
        }

        private void PlaceMarkButton_Click(object sender, EventArgs e)
        {
            markCollection collection = new markCollection();
            collection.token = userToken;
            collection.mark = new Mark();
            collection.mark.created_at = DateTime.Now;
            collection.mark.longitude = placeLongitude;
            collection.mark.latitude = placeLatitude;
            collection.mark.address = placeAddress.Text;
            collection.mark.type = placeTypeText;
            collection.mark.name = placeName.Text;
            HttpClientHandler httphandler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };
            HttpClient client = new HttpClient(httphandler);
            //client.MaxResponseContentBufferSize = 256000;
            Uri address = new Uri("https://tracker.hostingasp.pl/api/mark");
            var json = JsonConvert.SerializeObject(collection);
            Log.Debug("markActivity", json.ToString());
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            try
            {
                HttpResponseMessage response = client.PostAsync(address, content).Result;
                apiResponse respObject = JsonConvert.DeserializeObject<apiResponse>(response.Content.ReadAsStringAsync().Result);
                Log.Debug("markActivity", respObject.ToString());
                if (respObject.code == 200)
                {
                    Toast.MakeText(this, "Zgłoszenie zostało zapisane", ToastLength.Short).Show();
                    //this.deleteIncidents(this.getQueuedIncidents());
                }
                else if (respObject.code == 500)
                {
                    Toast.MakeText(this, respObject.message_pl, ToastLength.Short).Show();
                    //this.renewIncidents(this.getQueuedIncidents());
                }
                else
                {
                    // some unidentified problem occured
                }
            }
            catch (Exception connectionException)
            {
                Toast.MakeText(this, "Nie udało się zapisać. Sprawdź połączenie internetowe", ToastLength.Short).Show();
                // this.renewPositions(this.getQueuedPositions());
            }
            Finish();
        }

        public class peopleResponse
        {
            public int code { get; set; }
            public String message { get; set; }
            public List<_Place> places { get; set; }
        }

        public class apiResponse
        {
            public int code { get; set; }
            public String message { get; set; }
            public String message_pl { get; set; }
            public String token { get; set; }
            public int maxGpsAccuracy { get; set; }
            public int maxNetAccuracy { get; set; }
        }

        private class markCollection
        {
            public String token { get; set; }
            public Mark mark { get; set; }
        }

        private void getSpecialists()
        {
            askForSpecialists collection = new askForSpecialists();
            collection.token = userToken;
            collection.latitude = 51.2077831;// placeLatitude;
            collection.longitude = 16.1985254;// placeLongitude;
            HttpClientHandler httphandler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };
            HttpClient client = new HttpClient(httphandler);
            Uri address = new Uri("https://tracker.hostingasp.pl/api/SpecialistsPlaces");
            var json = JsonConvert.SerializeObject(collection);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            try
            {
                HttpResponseMessage response = client.PostAsync(address, content).Result;
                peopleResponse respObject = JsonConvert.DeserializeObject<peopleResponse>(response.Content.ReadAsStringAsync().Result);
                Log.Debug("people", respObject.message);
                if (respObject.code == 200)
                {
                    if(respObject.places.Any())
                    {

                    }
                    Toast.MakeText(this, "Zgłoszenie zostało zapisane", ToastLength.Short).Show();
                    //this.deleteIncidents(this.getQueuedIncidents());
                }
                else if (respObject.code == 500)
                {
                    Toast.MakeText(this, respObject.message, ToastLength.Short).Show();
                    //this.renewIncidents(this.getQueuedIncidents());
                }
                else
                {
                    // some unidentified problem occured
                }
            }
            catch (Exception connectionException)
            {
                Toast.MakeText(this, "Nie udało się zapisać. Sprawdź połączenie internetowe", ToastLength.Short).Show();
                // this.renewPositions(this.getQueuedPositions());
            }
        }

        private class askForSpecialists
        {
            public String token { get; set; }
            public Double latitude { get; set; }
            public Double longitude { get; set; }
        }

        public void OnLocationChanged(Android.Locations.Location location)
        {
            Geocoder geocoder = new Geocoder(this);
            IList<Address> addresses =  geocoder.GetFromLocation(location.Latitude,location.Longitude,1);
            Address address = addresses.FirstOrDefault();
            Log.Debug("markActivity",address.ToString());
            placeAddress.Text = address.GetAddressLine(0);
            placeLatitude = location.Latitude;
            placeLongitude = location.Longitude;
            showMarkPanel();
            hideLoadPanel();
            //throw new NotImplementedException();
        }

        public void OnProviderDisabled(string provider)
        {
            //throw new NotImplementedException();
        }

        public void OnProviderEnabled(string provider)
        {
            //throw new NotImplementedException();
        }

        public void OnStatusChanged(string provider, [GeneratedEnum] Availability status, Bundle extras)
        {
            //throw new NotImplementedException();
        }

        public void showMarkPanel()
        {
            placeTypeLabel.Visibility = Android.Views.ViewStates.Visible;
            placeType.Visibility = Android.Views.ViewStates.Visible;
            placeAddressLabel.Visibility = Android.Views.ViewStates.Visible;
            placeAddress.Visibility = Android.Views.ViewStates.Visible;
            placeNameLabel.Visibility = Android.Views.ViewStates.Visible;
            placeName.Visibility = Android.Views.ViewStates.Visible;
            placeMarkButton.Visibility = Android.Views.ViewStates.Visible;
        }

        public void hideMarkPanel()
        {
            placeTypeLabel.Visibility = Android.Views.ViewStates.Invisible;
            placeType.Visibility = Android.Views.ViewStates.Invisible;
            placeAddressLabel.Visibility = Android.Views.ViewStates.Invisible;
            placeAddress.Visibility = Android.Views.ViewStates.Invisible;
            placeNameLabel.Visibility = Android.Views.ViewStates.Invisible;
            placeName.Visibility = Android.Views.ViewStates.Invisible;
            placeMarkButton.Visibility = Android.Views.ViewStates.Invisible;
        }

        public void showLoadPanel()
        {
            placeLoadingLabel.Visibility = Android.Views.ViewStates.Visible;
            placeLoading.Visibility = Android.Views.ViewStates.Visible;
        }

        public void hideLoadPanel()
        {
            placeLoadingLabel.Visibility = Android.Views.ViewStates.Invisible;
            placeLoading.Visibility = Android.Views.ViewStates.Invisible;
        }

        public class _Place
        {
            public int id { get; set; }
            public string name { get; set; }
            public Double latitude { get; set; }
            public Double longitude { get; set; }
            public List<_Person> people { get; set; }
        }

        public class _Person
        {
            public int id { get; set; }
            public string name { get; set; }
            public string description { get; set; }
        }

    }
}