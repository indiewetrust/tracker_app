﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;
using System.Net;

namespace Location.Droid
{
    [Activity(Label = "Tracker - aktualizacja", Icon = "@drawable/icon")]
    public class UpdateActivity : Activity
    {
        TextView downloadText;
        ProgressBar downloadProgress;
        TextView downloadDescription;

        String updateApkUrl;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Update);
            // Create your application here

            this.updateApkUrl = Intent.GetStringExtra("updateApkUrl") ?? "Data not available";

            downloadText = FindViewById<TextView>(Resource.Id.downloadText);
            downloadProgress = FindViewById<ProgressBar>(Resource.Id.downloadProgress);
            downloadDescription = FindViewById<TextView>(Resource.Id.downloadDescription);

            downloadText.Text = "Trwa pobieranie aktualizacji...";
            downloadDescription.Text = "Po pobraniu paczki automatycznie uruchomi się okno z prośbą o zaktualizowanie aplikacji.";

            if (File.Exists(Android.OS.Environment.ExternalStorageDirectory + "/download/Tracker.apk"))
            {
                File.Delete(Android.OS.Environment.ExternalStorageDirectory + "/download/Tracker.apk");
            }
            DownloadFile(this.updateApkUrl, Android.OS.Environment.ExternalStorageDirectory + "/download/");
        }

        public void DownloadFile(string m_uri, string m_filePath)
        {
            var webClient = new WebClient();

            webClient.DownloadFileCompleted += (s, e) =>
            {
                Intent promptInstall = new Intent(Intent.ActionView).SetDataAndType(Android.Net.Uri.FromFile(new Java.IO.File(Android.OS.Environment.ExternalStorageDirectory + "/download/" + "Tracker.apk")), "application/vnd.android.package-archive");
                promptInstall.AddFlags(ActivityFlags.NewTask);

                StartActivity(promptInstall);
            };

            var url = new System.Uri(m_uri);
            webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadProgressCallback);
            webClient.DownloadFileAsync(url, Android.OS.Environment.ExternalStorageDirectory + "/download/Tracker.apk");

        }

        private void DownloadProgressCallback(object sender, DownloadProgressChangedEventArgs e)
        {
            int length = Convert.ToInt32(e.TotalBytesToReceive.ToString());
            int prog = Convert.ToInt32(e.BytesReceived.ToString());
            int perc = Convert.ToInt32(e.ProgressPercentage.ToString());
            downloadProgress.Progress = perc;
            downloadText.Text = String.Format("Pobrano: {0}%",e.ProgressPercentage);
        }
    }
}