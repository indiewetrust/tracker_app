﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Location.Droid
{
    [Activity(Label = "Specialists")]
    public class SpecialistsActivity : Activity
    {
        private SearchView _searchView;
        private ListView _listView;
        private ArrayAdapter _adapter;

        SpecialistsAdapter specAdapter;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Specialists);

            specAdapter = new SpecialistsAdapter(this);

            _listView = FindViewById<ListView>(Resource.Id.specialiststList);
            _searchView = FindViewById<SearchView>(Resource.Id.searchSpecialistList);
            _listView.Adapter = specAdapter;

            _listView.ItemClick += _listView_ItemClick;
        }

        private void _listView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            //Get our item from the list adapter
            var item = this.specAdapter.GetItemAtPosition(e.Position);

            //Make a toast with the item name just to show it was clicked 
            Toast.MakeText(this, item.id + " Clicked!", ToastLength.Short).Show();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.main, menu);

            var item = menu.FindItem(Resource.Id.action_search);

            var searchView = MenuItemCompat.GetActionView(item);
            _searchView = searchView.JavaCast<SearchView>();

            _searchView.QueryTextChange += (s, e) => _adapter.Filter.InvokeFilter(e.NewText);

            _searchView.QueryTextSubmit += (s, e) =>
            {
                // Handle enter/search button on keyboard here
                Toast.MakeText(this, "Searched for: " + e.Query, ToastLength.Short).Show();
                e.Handled = true;
            };

            MenuItemCompat.SetOnActionExpandListener(item, new SearchViewExpandListener(_adapter));

            return true;
        }
    }
}