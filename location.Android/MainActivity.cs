using System;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Util;
using Android.Locations;
using Location.Droid.Services;
using Android.Content.PM;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.IO;
using SQLite;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Plugin.Battery;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.IO.Compression;

namespace Location.Droid
{

	[Activity (Label = "Tracker", Icon = "@drawable/icon",LaunchMode = LaunchMode.SingleTop, MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.ScreenLayout)]
	public class MainActivity : Activity
	{
        // Default accuracy limits (changes after login)
        public int maxGpsAccuracy = 13;
        public int maxNetAccuracy = 42;
        public String userToken = "";
        public DateTime lastFetchedLocationTimestamp=DateTime.Now;

        //NotificationManager _notificationManager;
        readonly HttpClient _httpClient = new HttpClient();
        private JsonSerializer _serializer = new JsonSerializer();
        readonly Double appVersion = 14.0;
        private bool isAuthenticated = false;
        private bool isReporting = false;
        readonly string logTag = "MainActivity";
        readonly string dbFile = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "T07.db");

        readonly string OldDB1 = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "test01.db");
        readonly string OldDB2 = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "Beta.db");
        readonly string OldDB3 = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "Beta04.db");
        readonly string OldDB4 = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "Tracker_alpha.db");
        readonly string OldDB5 = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "T01.db"); // last release
        readonly string OldDB6 = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "T02.db");
        readonly string OldDB7 = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "T03.db");
        readonly string OldDB8 = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "T04.db");
        readonly string OldDB9 = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "T05.db");
        readonly string OldDB10 = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "T06.db");

        // make our labels
        EditText loginText;
        EditText passwordText;
        Button loginButton;
        Button markButton;
        Button markButtonLekarz;
        Button markButtonApteka;
        Button markButtonInne;
        Switch switchTrack;
        TextView reportInfo;
        Button reportButton;
        TextView info1;
        TextView info2;
        Intent startServiceIntent;
        Intent stopServiceIntent;
        bool isStarted = false;
        Button specialists;

        #region Lifecycle

        //Lifecycle stages
        protected async override void OnCreate (Bundle bundle)
		{
            base.OnCreate (bundle);
            Intent resumeMainActivity = new Intent(this, typeof(MainActivity));
            resumeMainActivity.AddFlags(ActivityFlags.ReorderToFront);
            StartActivity(resumeMainActivity);
            Log.Debug (logTag, "OnCreate: Location app is becoming active");

            if (bundle != null)
            {
                this.isAuthenticated = bundle.GetBoolean("isAuthenticated", false);
                this.isReporting = bundle.GetBoolean("isReporting", false);
                this.isStarted = bundle.GetBoolean(Constants.SERVICE_STARTED_KEY, false);
            }

            SetContentView (Resource.Layout.Main);
            OnNewIntent(this.Intent);

            startServiceIntent = new Intent(this, typeof(TimestampService));
            startServiceIntent.SetAction(Constants.ACTION_START_SERVICE);

            stopServiceIntent = new Intent(this, typeof(TimestampService));
            stopServiceIntent.SetAction(Constants.ACTION_STOP_SERVICE);

            // This event fires when the ServiceConnection lets the client (our App class) know that
            // the Service is connected. We use this event to start updating the UI with location
            // updates from the Service
            App.Current.LocationServiceConnected += (object sender, ServiceConnectedEventArgs e) =>
            {
                Log.Debug(logTag, "ServiceConnected Event Raised");
                // notifies us of location changes from the system
                App.Current.LocationService.LocationChanged += HandleLocationChanged;
                //notifies us of user changes to the location provider (ie the user disables or enables GPS)
                App.Current.LocationService.ProviderDisabled += HandleProviderDisabled;
                App.Current.LocationService.ProviderEnabled += HandleProviderEnabled;
                // notifies us of the changing status of a provider (ie GPS no longer available)
                App.Current.LocationService.StatusChanged += HandleStatusChanged;
            };

            loginText = FindViewById<EditText>(Resource.Id.loginText);
            passwordText = FindViewById<EditText>(Resource.Id.passwordText);
            loginButton = FindViewById<Button>(Resource.Id.loginButton);
            switchTrack = FindViewById<Switch>(Resource.Id.switchTrack);
            reportButton = FindViewById<Button>(Resource.Id.reportButton);
            reportInfo = FindViewById<TextView>(Resource.Id.reportInfo);
            info1 = FindViewById<TextView>(Resource.Id.info1);
            info2 = FindViewById<TextView>(Resource.Id.info2);
            markButton = FindViewById<Button>(Resource.Id.markButton);
            markButtonLekarz = FindViewById<Button>(Resource.Id.markButtonLekarz);
            markButtonApteka = FindViewById<Button>(Resource.Id.markButtonApteka);
            markButtonInne = FindViewById<Button>(Resource.Id.markButtonInne);
            markButton.Visibility = Android.Views.ViewStates.Invisible;
            specialists = FindViewById<Button>(Resource.Id.specialists);

            // Hide track panel
            this.hideTrackPanel();

            // Hide login form
            this.hideLoginForm();

            info1.Text = "Wersja aplikacji: "+this.appVersion.ToString().Replace(',','.');
            info2.Text = "";
            loginButton.Text = "Zaloguj mnie";
            // styling buttons
            loginButton.SetBackgroundColor(Android.Graphics.Color.ForestGreen);
            reportButton.Text = "Start";
            reportButton.SetBackgroundColor(Android.Graphics.Color.ForestGreen);
            markButton.SetBackgroundColor(Android.Graphics.Color.ForestGreen);
            markButtonLekarz.SetBackgroundColor(Android.Graphics.Color.ForestGreen);
            markButtonApteka.SetBackgroundColor(Android.Graphics.Color.ForestGreen);
            markButtonInne.SetBackgroundColor(Android.Graphics.Color.ForestGreen);
            reportInfo.Text = "Kliknij przycisk 'Start' aby rozpocz�� raportowanie pozycji urz�dzenia";

            loginButton.Click += LoginButton_Click;

            reportButton.Click += ReportButton_Click;

            markButton.Click += MarkButton_ClickAsync;

            specialists.Click += Specialists_ClickAsync;

            markButtonLekarz.Click += MarkButtonLekarz_ClickAsync;
            markButtonApteka.Click += MarkButtonApteka_ClickAsync;
            markButtonInne.Click += MarkButtonInne_ClickAsync;

            // Start the location service:
            App.StartLocationService();

            // Updates
            this.checkForUpdates();

            if(File.Exists(OldDB1))
            {
                File.Delete(OldDB1);
            }
            if (File.Exists(OldDB2))
            {
                File.Delete(OldDB2);
            }
            if (File.Exists(OldDB3))
            {
                File.Delete(OldDB3);
            }
            if (File.Exists(OldDB4))
            {
                File.Delete(OldDB4);
            }
            if (File.Exists(OldDB5))
            {
                File.Delete(OldDB5);
            }

            // Create SQLite database if not exists
            if (!File.Exists(dbFile))
            {
                var db = new SQLiteConnection(dbFile);
                db.CreateTable<Position>();
                db.CreateTable<Setting>();
                db.CreateTable<Incident>();
                db.CreateTable<_specialist>();
            }
            //} else
            //{
            //    if (tableIncidentExists()==false)
            //    {
            //        var db = new SQLiteConnection(dbFile);
            //        db.CreateTable<Incident>();
            //    }
            //}
 
            Setting settings = await getSettings();
            if(!String.IsNullOrEmpty(settings.userName)&&!String.IsNullOrEmpty(settings.userPass))
            {
                loginText.Text = settings.userName;
                passwordText.Text = settings.userPass;

                AuthData userData = new AuthData();
                userData.name = settings.userName;
                userData.pass = settings.userPass;
                userData.version = appVersion.ToString();

                await AuthMe(userData);
            } else
            {
                showLoginForm();
            }


            // Send positions every x minutes
            var startTimeSpan = TimeSpan.Zero;
            var periodTimeSpan = TimeSpan.FromMinutes(Constants.REPORT_LOCATIONS_INTERVAL);
            bool goSend = false;

            var timer = new System.Threading.Timer(async (e) =>
            {
                if (goSend == true)
                {
                    syncPositions();
                    // Creating incidents collection
                    syncIncidents();
                    // reset location service if needed
                    if ((DateTime.Now.Subtract(lastFetchedLocationTimestamp).TotalSeconds > Constants.RESTART_LOCATION_SERVICE_IF_IDLE_FOR_MORE_THAN) && (switchTrack.Checked))
                    {
                        App.StopLocationService();
                        App.StartLocationService();
                        Log.Debug(logTag, "Location service restarted");
                        Incident incident = new Incident();
                        incident.latitude = 0;
                        incident.longitude = 0;
                        incident.type = "Location";
                        incident.description = "Us�uga lokalizacji zrestartowana";
                        incident.status = "new";
                        incident.created_at = DateTime.Now;
                        await saveIncident(incident);
                        //Toast.MakeText(this, "Restart location service", ToastLength.Short).Show();
                    }
                }
                goSend = true;
            }, null, startTimeSpan, periodTimeSpan);

            if (!this.checkLocationEnabled())
            {
                this.showLocationNeeded();
            }

            // Start foreground service
            StartService(startServiceIntent);
        }

        private async void Specialists_ClickAsync(object sender, EventArgs e)
        {
            var specialistsWindow = new Intent(this, typeof(SpecialistsActivity));
            specialistsWindow.PutExtra("userToken", await getToken());
            specialistsWindow.PutExtra("placeType", "Lekarz");
            StartActivity(specialistsWindow);
        }

        private async void MarkButtonInne_ClickAsync(object sender, EventArgs e)
        {
            var markWindow = new Intent(this, typeof(MarkActivity));
            markWindow.PutExtra("userToken", await getToken());
            markWindow.PutExtra("placeType", "Inne");
            StartActivity(markWindow);
        }

        private async void MarkButtonApteka_ClickAsync(object sender, EventArgs e)
        {
            var markWindow = new Intent(this, typeof(MarkActivity));
            markWindow.PutExtra("userToken", await getToken());
            markWindow.PutExtra("placeType", "Apteka");
            StartActivity(markWindow);
        }

        private async void MarkButtonLekarz_ClickAsync(object sender, EventArgs e)
        {
            var markWindow = new Intent(this, typeof(MarkActivity));
            markWindow.PutExtra("userToken", await getToken());
            markWindow.PutExtra("placeType", "Lekarz");
            StartActivity(markWindow);
        }

        private async void MarkButton_ClickAsync(object sender, EventArgs e)
        {
            var markWindow = new Intent(this, typeof(MarkActivity));
            markWindow.PutExtra("userToken", await getToken());
            StartActivity(markWindow);
        }

        protected override void OnNewIntent(Intent intent)
        {
            if (intent == null)
            {
                return;
            }

            var bundle = intent.Extras;
            if (bundle != null)
            {
                if (bundle.ContainsKey(Constants.SERVICE_STARTED_KEY))
                {
                    isStarted = true;
                }
            }
        }

        private async void ReportButton_Click(object sender, EventArgs e)
        {
            if (this.switchTrack.Checked)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.SetTitle("Koniec raportowania");
                builder.SetMessage("Czy na pewno chcesz zako�czy� raportowanie?");
                builder.SetNegativeButton("Anuluj", (senderAlert, args) =>
                {

                });
                builder.SetPositiveButton("Zako�cz", async (senderAlert, args) =>
                {
                    this.switchTrack.Toggle();
                    this.isReporting = false;
                    this.reportButton.Text = "Start";
                    reportButton.SetBackgroundColor(Android.Graphics.Color.ForestGreen);
                    reportInfo.Text = "Kliknij przycisk 'Start' aby rozpocz�� raportowanie pozycji urz�dzenia";
                    Incident incident = new Incident();
                    incident.latitude = 0;
                    incident.longitude = 0;
                    incident.type = "Reporting";
                    incident.description = "Wci�ni�cie przycisku Koniec";
                    incident.status = "new";
                    incident.created_at = DateTime.Now;
                    await saveIncident(incident);
                });
                Dialog alertDialog = builder.Create();
                alertDialog.SetCanceledOnTouchOutside(false);
                alertDialog.Show();
            } else
            {
                this.switchTrack.Toggle();
            }

            if (this.switchTrack.Checked)
            {
                this.isReporting = true;
                this.reportButton.Text = "Koniec";
                reportButton.SetBackgroundColor(Android.Graphics.Color.DarkRed);
                reportInfo.Text = "Kliknij przycisk 'Koniec' aby zako�czy� raportowanie pozycji urz�dzenia";
                Incident incident = new Incident();
                incident.latitude = 0;
                incident.longitude = 0;
                incident.type = "Reporting";
                incident.description = "Wci�ni�cie przycisku Start";
                incident.status = "new";
                incident.created_at = DateTime.Now;
                await saveIncident(incident);
            } else
            {
                this.isReporting = false;
                this.reportButton.Text = "Start";
                reportButton.SetBackgroundColor(Android.Graphics.Color.ForestGreen);
                reportInfo.Text = "Kliknij przycisk 'Start' aby rozpocz�� raportowanie pozycji urz�dzenia";
                Incident incident = new Incident();
                incident.latitude = 0;
                incident.longitude = 0;
                incident.type = "Reporting";
                incident.description = "Wci�ni�cie przycisku Koniec";
                incident.status = "new";
                incident.created_at = DateTime.Now;
                await saveIncident(incident);
            }
        }

        private async void getSpecialists()
        {
            specialistsRequest sr = new specialistsRequest();
            sr.token = await getToken();
            sr.updated_after = await getLastSpecialistUpdate();
            Log.Debug(logTag, "Getting specialists");
            // Handling WebResponse
            HttpClientHandler httphandler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };
            HttpClient client = new HttpClient(httphandler);
            //client.MaxResponseContentBufferSize = 256000;
            Uri address = new Uri("https://tracker.hostingasp.pl/api/specialists");
            var json = JsonConvert.SerializeObject(sr);
            Log.Debug("Request specialists", json);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            try
            {
                HttpResponseMessage response = await client.PostAsync(address, content);
                specialistsResponse respObject = JsonConvert.DeserializeObject<specialistsResponse>(await response.Content.ReadAsStringAsync());
                if (response.IsSuccessStatusCode)
                {
                    if (respObject.code == 200)
                    {
                        Setting s = await getSettings();
                        s.last_specialist_update = DateTime.Now;
                        await updateSettings(s);
                        if(respObject.specialists.Any())
                        {
                            await saveSpecialists(respObject.specialists);
                            //foreach(_specialist specialist in respObject.specialists)
                            //{
                            //    await saveSpecialist(specialist);
                            //}
                            Handler handler = new Handler(Looper.MainLooper);
                            Action myAction = () =>
                            {
                                Toast.MakeText(this, String.Format("Zaktualizowano baz� lekarzy (rekord�w: {0} | zapisano {1})", respObject.specialists.Count, getNumberOfSpecialists()), ToastLength.Short).Show();
                            };
                            handler.Post(myAction);
                        }
                    }
                    else
                    {
                        Handler handler = new Handler(Looper.MainLooper);
                        Action myAction = () =>
                        {
                            Toast.MakeText(this, String.Format("Error getting specialists"), ToastLength.Short).Show();
                        };
                        handler.Post(myAction);
                    }
                }
            }
            catch (Exception connectionException)
            {
                //Toast.MakeText(this, connectionException.Message, ToastLength.Long).Show();
            }
        }

        private async void checkForUpdates()
        {
            // Handling WebResponse
            HttpClientHandler httphandler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };
            HttpClient client = new HttpClient(httphandler);
            //client.MaxResponseContentBufferSize = 256000;
            Uri address = new Uri("https://tracker.hostingasp.pl/api/distribution");
            try
            {
                HttpResponseMessage response = await client.GetAsync(address);
                updateResponse respObject = JsonConvert.DeserializeObject<updateResponse>(await response.Content.ReadAsStringAsync());
                if (response.IsSuccessStatusCode)
                {
                    if(respObject.code==200)
                    {
                        if (this.appVersion<respObject.version)
                        {
                            AlertDialog.Builder builder = new AlertDialog.Builder(this);
                            builder.SetTitle("Aktualizacja");
                            builder.SetMessage("Dost�pna jest nowa wersja aplikacji. Aktualizacja jest konieczna aby korzysta� z trackera.");
                            builder.SetPositiveButton("Pobierz", (senderAlert, args) =>
                            {
                                var updateWindow = new Intent(this, typeof(UpdateActivity));
                                updateWindow.PutExtra("updateApkUrl", respObject.apkUrl.ToString());
                                StartActivity(updateWindow);
                            });
                            Dialog alertDialog = builder.Create();
                            alertDialog.SetCanceledOnTouchOutside(false);
                            alertDialog.Show();
                        }
                    } else
                    {

                    }
                }
            }
            catch (Exception connectionException)
            {
                //Toast.MakeText(this, connectionException.Message, ToastLength.Long).Show();
            }
        }

        public override void OnBackPressed()
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.SetTitle("Potwierd� zamkni�cie");
            builder.SetMessage("Czy na pewno chcesz zako�czy� dzia�anie trackera?");
            builder.SetNegativeButton("Anuluj", (senderAlert, args) =>
            {

            });
            builder.SetPositiveButton("Wy��cz", (senderAlert, args) =>
            {
                closeApplication();
            });
            Dialog alertDialog = builder.Create();
            alertDialog.SetCanceledOnTouchOutside(false);
            alertDialog.Show();
        }

        public void closeApplication()
        {
            var activity = (Activity)this;
            activity.FinishAffinity();
        }

        private void showLocationNeeded()
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.SetTitle("W��cz lokalizacj�");
            builder.SetMessage("Nale�y w��czy� us�ug� lokalizacji w telefonie, aby mie� dost�p do aktualnego po�o�enia.");
            builder.SetPositiveButton("W��cz lokalizacj�", (senderAlert, args) =>
            {
                Intent intent = new Intent(Android.Provider.Settings.ActionLocationSourceSettings);
                StartActivity(intent);
            });
            Dialog alertDialog = builder.Create();
            alertDialog.SetCanceledOnTouchOutside(false);
            alertDialog.Show();
        }

        private int checkBatteryLevel()
        {
            int batteryLevel = CrossBattery.Current.RemainingChargePercent;
            return batteryLevel;
        }

        private bool checkLocationEnabled()
        {
            bool locationEnabled = false;
            LocationManager LocMgr = Android.App.Application.Context.GetSystemService("location") as LocationManager;
            if(LocMgr.IsProviderEnabled(LocationManager.GpsProvider)||LocMgr.IsProviderEnabled(LocationManager.NetworkProvider))
            {
                locationEnabled = true;
            }
            return locationEnabled;
        }

        private async void syncIncidents()
        {
            IncidentsCollection ic = new IncidentsCollection();
            ic.incidents = await getAndQueueIncidentsToSend();
            ic.token = await getToken();
            HttpClientHandler httphandler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };
            HttpClient client = new HttpClient(httphandler);
            //client.MaxResponseContentBufferSize = 256000;
            Uri address = new Uri("https://tracker.hostingasp.pl/api/incident");
            var json = JsonConvert.SerializeObject(ic);
            Log.Debug("API - incidents", json);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            try
            {
                HttpResponseMessage response = await client.PostAsync(address, content);
                apiResponse respObject = JsonConvert.DeserializeObject<apiResponse>(await response.Content.ReadAsStringAsync());
                //Handler handler = new Handler(Looper.MainLooper);
                //Action myAction = () =>
                //{
                //    Toast.MakeText(this, respObject.message_pl, ToastLength.Long).Show();

                //};
                //handler.Post(myAction);
                if (respObject.code == 200)
                {
                    await deleteIncidents(await getQueuedIncidents());
                }
                else if (respObject.code == 500)
                {
                    await renewIncidents(await getQueuedIncidents());
                }
                else
                {
                    // some unidentified problem occured
                }
            }
            catch (Exception connectionException)
            {
                await renewPositions(await getQueuedPositions());
            }
        }

        private async void syncPositions()
        {
            PositionsCollection pc = new PositionsCollection();
            pc.positions = await getAndQueuePositionsToSend();
            pc.token = await getToken();
            HttpClientHandler httphandler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };
            HttpClient client = new HttpClient(httphandler);
            //client.MaxResponseContentBufferSize = 256000;
            Uri address = new Uri("https://tracker.hostingasp.pl/api/position");
            var json = JsonConvert.SerializeObject(pc);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            try
            {
                HttpResponseMessage response = await client.PostAsync(address, content);
                apiResponse respObject = JsonConvert.DeserializeObject<apiResponse>(await response.Content.ReadAsStringAsync());
                if (switchTrack.Checked)
                {
                    Handler handler = new Handler(Looper.MainLooper);
                    Action myAction = () =>
                    {
                        //Toast.MakeText(this, respObject.message_pl.ToString(), ToastLength.Long).Show();
                        info2.Text = "Ostatnia synchronizacja: " + DateTime.Now.ToShortTimeString();

                    };
                    handler.Post(myAction);
                }
                Log.Debug("API - positions response", JsonConvert.SerializeObject(respObject));
                if (respObject.code==200)
                {
                    await deletePositions(await getQueuedPositions());
                } else
                {
                    Log.Debug("API","Try autologin");
                    await renewPositions(await getQueuedPositions());
                    // try autologin

                    if(await existsSettings())
                    {
                        Log.Debug("API", "Try autologin | settings exists");
                        Setting s = await getSettings();
                        AuthData userData = new AuthData();
                        userData.name = s.userName;
                        userData.pass = s.userPass;
                        userData.version = appVersion.ToString();
                        Log.Debug("API", JsonConvert.SerializeObject(userData));
                        bool authResult = await AuthMe(userData);

                        if(authResult==true)
                        {
                            Log.Debug("API", "Try autologin | auth success");
                        } else
                        {
                            Log.Debug("API", "Try autologin | auth error");
                            this.showLoginForm();
                            this.hideTrackPanel();
                        }
                    } else
                    {
                        Log.Debug("API", "Try autologin | settings not exists");
                        this.showLoginForm();
                        this.hideTrackPanel();
                    }

                } 
            }
            catch (Exception connectionException)
            {
                await renewPositions(await getQueuedPositions());
            }
        }

        private async void LoginButton_Click(object sender, EventArgs e)
        { 
            // Get&Set user auth data
            AuthData userData = new AuthData();
            userData.name = loginText.Text;
            userData.pass = passwordText.Text;
            userData.version = appVersion.ToString();

            // auth User
            await AuthMe(userData);
        }

        public async Task<bool> AuthMe(AuthData authData)
        {
            bool authResult = false;
            Log.Debug("API", JsonConvert.SerializeObject(authData));
            if (authData!=null)
            {
                Log.Debug("API", "authDataOk");
                Uri address = new Uri("https://tracker.hostingasp.pl/api/auth");
                var json = JsonConvert.SerializeObject(authData);
                Log.Debug("API", json);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                try
                {
                    Log.Debug("API", "HttpClient");
                    HttpClient client = new HttpClient();
                    HttpResponseMessage response = await client.PostAsync(address, content);
                    Log.Debug("API", JsonConvert.SerializeObject(response));
                    apiResponse respObject = JsonConvert.DeserializeObject<apiResponse>(await response.Content.ReadAsStringAsync());
                    //Toast.MakeText(this, respObject.message_pl.ToString(), ToastLength.Short).Show();
                    Log.Debug("AuthMe - response object", JsonConvert.SerializeObject(respObject));
                    if (response.IsSuccessStatusCode)
                    {
                        //this.userToken = respObject.token;
                        Log.Debug("AuthMe - success", respObject.ToString());
                        if (await existsSettings())
                        {
                            Setting s = await getSettings();
                            s.userName = loginText.Text;
                            s.userPass = passwordText.Text;
                            s.userToken = respObject.token;
                            await updateSettings(s);
                        }
                        else
                        {
                            Setting setting = new Setting();
                            setting.version = appVersion;
                            setting.userName = loginText.Text;
                            setting.userPass = passwordText.Text;
                            setting.userToken = respObject.token;
                            await saveSettings(setting);
                        }
                        getSpecialists();
                        Log.Debug("Token", await getToken());
                        this.hideLoginForm();
                        this.showTrackPanel();
                        this.isAuthenticated = true;
                        Incident incident = new Incident();
                        incident.latitude = 0;
                        incident.longitude = 0;
                        incident.type = "Authentication";
                        incident.description = "Poprawne zalogowanie";
                        incident.status = "new";
                        incident.created_at = DateTime.Now;
                        await saveIncident(incident);
                        this.maxGpsAccuracy = respObject.maxGpsAccuracy;
                        this.maxNetAccuracy = respObject.maxNetAccuracy;
                        authResult = true;
                    } else
                    {
                        this.showLoginForm();
                        this.hideTrackPanel();
                        this.isAuthenticated = false;
                        authResult = false;
                    }
                }
                catch (Exception connectionException)
                {
                    Log.Debug("API", "showLogin");
                    this.showLoginForm();
                    Toast.MakeText(this, "B��d po��czenia z Internetem. W��cz transmisj� danych kom�rkowych.", ToastLength.Long).Show();
                }
            } else
            {
                Log.Debug("API", "authDataBad");
                authResult = false;
            }

            return authResult;
        }

        public class specialistsRequest
        {
            public String token { get; set; }
            public DateTime? updated_after { get; set; }
        }

        public class apiResponse
        {
            public int code { get; set; }
            public String message { get; set; }
            public String message_pl { get; set; }
            public String token { get; set; }
            public int maxGpsAccuracy { get; set; }
            public int maxNetAccuracy { get; set; }
        }

        public class specialistsResponse
        {
            public int code { get; set; }
            public List<_specialist> specialists { get; set; }
        }

        public class _specialist
        {
            public int id { get; set; }
            public string name { get; set; }
            public string surname { get; set; }
            public string description { get; set; }
            public string status { get; set; }
            public bool? is_target { get; set; }
            public DateTime? scheduled_at { get; set; }
            public DateTime? completed_at { get; set; }
        }

        public class updateResponse
        {
            public int code { get; set; }
            public Double version { get; set; }
            public System.Uri apkUrl {get;set;} 
            public String message_pl { get; set; }
        }

        private void hideLoginForm()
        {
            loginText.Visibility = Android.Views.ViewStates.Invisible;
            passwordText.Visibility = Android.Views.ViewStates.Invisible;
            loginButton.Visibility = Android.Views.ViewStates.Invisible;
        }

        private void showLoginForm()
        {
            loginText.Visibility = Android.Views.ViewStates.Visible;
            passwordText.Visibility = Android.Views.ViewStates.Visible;
            loginButton.Visibility = Android.Views.ViewStates.Visible;
        }

        private void hideTrackPanel()
        {
            switchTrack.Visibility = Android.Views.ViewStates.Invisible;
            reportInfo.Visibility = Android.Views.ViewStates.Invisible;
            reportButton.Visibility = Android.Views.ViewStates.Invisible;
            markButton.Visibility = Android.Views.ViewStates.Invisible;
            markButtonLekarz.Visibility = Android.Views.ViewStates.Invisible;
            markButtonApteka.Visibility = Android.Views.ViewStates.Invisible;
            markButtonInne.Visibility = Android.Views.ViewStates.Invisible;
        }

        private void showTrackPanel()
        {
            //switchTrack.Visibility = Android.Views.ViewStates.Visible;
            reportInfo.Visibility = Android.Views.ViewStates.Visible;
            reportButton.Visibility = Android.Views.ViewStates.Visible;
            markButton.Visibility = Android.Views.ViewStates.Invisible;
            markButtonLekarz.Visibility = Android.Views.ViewStates.Visible;
            markButtonApteka.Visibility = Android.Views.ViewStates.Visible;
            markButtonInne.Visibility = Android.Views.ViewStates.Visible;
        }

        protected override void OnPause()
		{
			Log.Debug (logTag, "OnPause: Location app is moving to background");
			base.OnPause();
        }


        protected override void OnResume()
        {
            Log.Debug(logTag, "OnResume: Location app is moving into foreground");
            base.OnResume();
            Handler handler1 = new Handler(Looper.MainLooper);
            Action myAction = () =>
            {
                if (this.isAuthenticated == true)
                {
                    this.hideLoginForm();
                    this.showTrackPanel();
                }
                if (this.isReporting == true)
                {
                    this.reportButton.Text = "Koniec";
                    reportButton.SetBackgroundColor(Android.Graphics.Color.DarkRed);
                    reportInfo.Text = "Kliknij przycisk 'Koniec' aby zako�czy� raportowanie pozycji urz�dzenia";
                }
            };
            handler1.Post(myAction);
        }
		
		protected override void OnDestroy ()
		{
            Log.Debug(logTag, "OnDestroy: Location app is becoming inactive");

            Incident incident = new Incident();
            incident.latitude = 0;
            incident.longitude = 0;
            incident.type = "Application";
            incident.description = "Aplikacja zosta�a wy��czona";
            incident.status = "new";
            incident.created_at = DateTime.Now;
            this.saveIncident(incident);

            syncIncidents();

            syncPositions();

            // Stop the location service:
            App.StopLocationService();

            // Stop the notification service
            StopService(stopServiceIntent);

            base.OnDestroy ();
        }

        protected override void OnRestoreInstanceState(Bundle savedInstanceState)
        {
            base.OnRestoreInstanceState(savedInstanceState);
        }

        protected override void OnSaveInstanceState(Bundle outState)
        {
            outState.PutBoolean("isAuthenticated", this.isAuthenticated);
            outState.PutBoolean("isReporting", this.isReporting);
            outState.PutBoolean(Constants.SERVICE_STARTED_KEY, isStarted);
            base.OnSaveInstanceState(outState);
        }

        public class AuthData
        {
            public String name { get; set; }
            public String pass { get; set; }
            public String version { get; set; }
        }

        public class JsonContent : HttpContent
        {
            private JsonSerializer serializer { get; }
            private object value { get; }

            public JsonContent(object value)
            {
                this.serializer = new JsonSerializer();
                this.value = value;
                Headers.ContentType = new MediaTypeHeaderValue("application/json");
                Headers.ContentEncoding.Add("gzip");
            }

            protected override bool TryComputeLength(out long length)
            {
                length = -1;
                return false;
            }

            protected override Task SerializeToStreamAsync(Stream stream, TransportContext context)
            {
                return Task.Factory.StartNew(() =>
                {
                    using (var gzip = new GZipStream(stream, CompressionMode.Compress, true))
                    using (var writer = new StreamWriter(gzip))
                    {
                        serializer.Serialize(writer, value);
                    }
                });
            }
        }

        #endregion

        #region SQLite

        private async Task<bool> tableIncidentExists()
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                await db.Table<Incident>().CountAsync();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private async Task<bool> saveIncident(Incident i)
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                await db.InsertAsync(i);
                //Toast.MakeText(this, statsIncidents(), ToastLength.Long).Show();
                return true;
            }
            catch (Exception e)
            {
                //Toast.MakeText(this, e.Message, ToastLength.Long).Show();
                return false;
            }
        }

        private async Task<int> saveSettings(Setting s)
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                await db.InsertAsync(s);
                return 1;
            } catch(Exception e)
            {
                return 0;
            }
        }

        private async Task<int> updateSettings(Setting s)
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                await db.UpdateAsync(s);
                return 1;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        private async Task<bool> existsSettings()
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                int count = await db.Table<Setting>().CountAsync();
                if(count>0)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private async Task<Setting> getSettings()
        {
            Setting s = new Setting();
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                s = await db.GetAsync<Setting>(1);
            }
            catch (Exception e)
            {

            }
            return s;
        }

        private async Task<String> getToken()
        {
            String token = "";
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                var response = await db.GetAsync<Setting>(1);
                token = response.userToken;
            }
            catch (Exception e)
            {

            }
            //token = userToken;
            return token;
        }

        private async Task<DateTime> getLastSpecialistUpdate()
        {
            DateTime _lsu = Convert.ToDateTime("1900-01-01");
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                var response = await db.GetAsync<Setting>(1);
                if (response.last_specialist_update != null)
                {
                    _lsu = Convert.ToDateTime(response.last_specialist_update);
                }
            } catch
            {

            }
            return _lsu;
        }

        private async Task<Position> getLastPosition()
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                Position lastPosition = await db.Table<Position>().OrderBy(p => p.created_at).Take(1).FirstOrDefaultAsync();
                return lastPosition;
            }
            catch (Exception e)
            {
                return new Position();
            }
        }

        private async Task<bool> saveSpecialists(List<_specialist> specs)
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                await db.InsertAllAsync(specs);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private async Task<bool> saveSpecialist(_specialist s)
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                await db.InsertAsync(s);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private async Task<int> getNumberOfSpecialists()
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                return await db.Table<_specialist>().CountAsync();
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        private async Task<bool> savePosition(Position p)
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                await db.InsertAsync(p);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private async Task<bool> renewIncidents(List<Incident> incidents)
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                if (incidents.Any())
                {
                    foreach (Incident i in incidents)
                    {
                        i.status = "new";
                        await db.UpdateAsync(i);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private async Task<bool> renewPositions(List<Position> positions)
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                if (positions.Any())
                {
                    foreach (Position p in positions)
                    {
                        p.status = "new";
                        await db.UpdateAsync(p);
                    }
                }
                return true;
            } catch (Exception e)
            {
                return false;
            }
        }

        private async Task<bool> deleteIncidents(List<Incident> incidents)
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                if (incidents.Any())
                {
                    foreach (Incident i in incidents)
                    {
                        await db.DeleteAsync(i);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private async Task<bool> deletePositions(List<Position> positions)
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                if (positions.Any())
                {
                    foreach (Position p in positions)
                    {
                        await db.DeleteAsync(p);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private async Task<List<Position>> getQueuedPositions()
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                List<Position> positions = await db.Table<Position>().Where(p => p.status == "queue").OrderBy(p => p.created_at).ToListAsync();
                return positions;
            }
            catch (Exception e)
            {
                return new List<Position>();
            }
        }

        private async Task<List<Incident>> getQueuedIncidents()
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                List<Incident> incidents = await db.Table<Incident>().Where(i => i.status == "queue").OrderBy(i => i.created_at).ToListAsync();
                return incidents;
            }
            catch (Exception e)
            {
                return new List<Incident>();
            }
        }

        private async Task<int> getNumberOfQueuedPositions()
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                return await db.Table<Position>().CountAsync();
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        private async Task<List<CleanIncident>> getAndQueueIncidentsToSend()
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                List<Incident> incidents = await db.Table<Incident>().Where(i => i.status == "new").OrderBy(i => i.created_at).ToListAsync();
                List<CleanIncident> cleanIncidents = new List<CleanIncident>();
                if (incidents.Any())
                {
                    foreach (Incident i in incidents)
                    {
                        i.status = "queue";
                        await db.UpdateAsync(i);
                        CleanIncident ci = new CleanIncident();
                        ci.latitude = 0;
                        ci.longitude = 0;
                        ci.type = i.type;
                        ci.description = i.description;
                        ci.created_at = i.created_at;
                        cleanIncidents.Add(ci);
                    }
                }
                return cleanIncidents;
            }
            catch (Exception e)
            {
                return new List<CleanIncident>();
            }
        }

        private async Task<List<CleanPosition>> getAndQueuePositionsToSend()
        {
            try
            {
                var db = new SQLiteAsyncConnection(dbFile);
                // get max 1000 positions from saved geolocation...
                List<Position> positions = await db.Table<Position>().Where(p=>p.status=="new").OrderBy(p=>p.created_at).Take(1000).ToListAsync();
                List<CleanPosition> cleanPositions = new List<CleanPosition>();
                if(positions.Any())
                {
                    foreach(Position p in positions)
                    {
                        p.status = "queue";
                        await db.UpdateAsync(p);
                        CleanPosition cp = new CleanPosition();
                        cp.latitude = p.latitude;
                        cp.longitude = p.longitude;
                        cp.speed = p.speed;
                        cp.accuracy = Convert.ToInt32(p.accuracy);
                        cp.battery = p.battery;
                        cp.provider = p.provider;
                        cp.created_at = p.created_at;
                        cleanPositions.Add(cp);
                    }
                }
                return cleanPositions;
            }
            catch (Exception e)
            {
                return  new List<CleanPosition>();
            }
        }

        private String statsSettings()
        {
            try
            {
                var db = new SQLiteConnection(dbFile);
                List<Setting> settings = db.Table<Setting>().ToList<Setting>();
                return JsonConvert.SerializeObject(settings).ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        private String statsPositions()
        {
            try
            {
                var db = new SQLiteConnection(dbFile);
                List<Position> positions = db.Table<Position>().ToList<Position>();
                int newRecords = positions.Where(p => p.status == "new").Count();
                int queueRecords = positions.Where(p=>p.status=="queue").Count();
                int sentRecords = positions.Where(p => p.status == "saved").Count();
                return "new("+ newRecords.ToString() + ") queue("+ queueRecords.ToString() + ") sent("+ sentRecords.ToString() + ")";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        private String statsIncidents()
        {
            try
            {
                var db = new SQLiteConnection(dbFile);
                List<Incident> incidents = db.Table<Incident>().ToList<Incident>();
                int newRecords = incidents.Where(p => p.status == "new").Count();
                int queueRecords = incidents.Where(p => p.status == "queue").Count();
                int sentRecords = incidents.Where(p => p.status == "saved").Count();
                return "new(" + newRecords.ToString() + ") queue(" + queueRecords.ToString() + ") sent(" + sentRecords.ToString() + ")";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        #endregion

        #region Android Location Service methods

        public void HandleLocationChanged(object sender, LocationChangedEventArgs e)
		{
			Android.Locations.Location location = e.Location;

            // save location in local sqllite file only if reporting is 'on' and accuracy is in defined limit
            if (switchTrack.Checked && ( (location.Provider=="gps"&&location.Accuracy<maxGpsAccuracy) || (location.Provider=="network"&&location.Accuracy<maxNetAccuracy) || location.Provider=="fused" ) )
            {
                Position position = new Position();
                position.latitude = location.Latitude;
                position.longitude = location.Longitude;
                position.provider = location.Provider;
                position.speed = location.Speed;
                position.accuracy = location.Accuracy;
                position.battery = this.checkBatteryLevel();
                position.status = "new";
                position.created_at = DateTime.Now;
                this.savePosition(position);
                this.lastFetchedLocationTimestamp = DateTime.Now;
                //Toast.MakeText(this, location.Provider.ToString() +" | "+location.Accuracy.ToString()+" | "+ DateTime.Now.ToShortTimeString(), ToastLength.Short).Show();
            }
            //Toast.MakeText(this, location.Provider.ToString() + " | " + location.Accuracy.ToString() + " | " + DateTime.Now.ToShortTimeString(), ToastLength.Short).Show();
        }

        public void HandleProviderDisabled(object sender, ProviderDisabledEventArgs e)
		{
            if (e.Provider == "gps")
            {
                Incident incident = new Incident();
                incident.latitude = 0;
                incident.longitude = 0;
                incident.type = "Location";
                incident.description = "Wy��czono us�ug� lokalizacji";
                incident.status = "new";
                incident.created_at = DateTime.Now;
                this.saveIncident(incident);
                this.showLocationNeeded();
            }
        }

        public void HandleProviderEnabled(object sender, ProviderEnabledEventArgs e)
		{
            if (e.Provider == "gps")
            {
                Incident incident = new Incident();
                incident.latitude = 0;
                incident.longitude = 0;
                incident.type = "Location";
                incident.description = "W��czono us�ug� lokalizacji";
                incident.status = "new";
                incident.created_at = DateTime.Now;
                this.saveIncident(incident);
            }
        }

		public void HandleStatusChanged(object sender, StatusChangedEventArgs e)
		{

        }

        #endregion

    }
}


